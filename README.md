# yoda-minify #

Minify javascript files for YODA.

### Usage ###

First, install `gulp` as globally.

```
#!shell

npm install gulp -g
```

Then, install npm packages as a local dependency.

```
#!shell

npm install
```

Target files move to `./src` folder and minify it.

```
#!shell

gulp minify
```

Or, watch files modified and automatically minify them all.

```
#!shell

gulp watch
```
