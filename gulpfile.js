var gulp = require('gulp');
var compress = require('gulp-yuicompressor');
var packer = require('gulp-packer');
var streamify = require('gulp-streamify');
var clean = require('gulp-clean');

var config = {
  src: './src',
  dest: './build',
};

gulp.task('clean', function() {
  return gulp.src(config.dest, { read: false })
    .pipe(clean());
});

gulp.task('minify', function() {
  return gulp.src(config.src + '/**/*.js')
    .pipe(compress({ type: 'js' }))
    .pipe(streamify(packer({ base62: true, shrink: true })))
    .pipe(gulp.dest(config.dest));
});

gulp.task('watch', function() {
  gulp.watch(config.src + '/**/*.js', ['minify']);
});

gulp.task('default', ['clean', 'minify']);
